# Minimal Password Manager

## Too Long; Didn't Read

1. Fully offline
2. Doesn't use external APIs
3. Doesn't use external assets like images, stylesheets and scripts
4. Uses [IndexedDB API](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)
   as a database backend
5. Uses [Crypto](https://developer.mozilla.org/en-US/docs/Web/API/Crypto/subtle)
   for encryption and decryption behind the scenes
6. I personally recommend running it using the following ways to
   prevent online security breaches:
   1. Running locally from a `file://` URL ([See Releases](#releases))
   2. Running on your personal server
   3. Using an existing website that **YOU TRUST** ([example website](https://compro-prasad.gitlab.io/minimal-password-manager/))
7. Built using [Svelte](https://svelte.dev/) + [Vite](https://vitejs.dev/)

## Releases
1. Page: https://gitlab.com/Compro-Prasad/minimal-password-manager/-/releases
2. Download the build files from the URL denoted by `Build file`
3. Start using it in your browser

## TODO
1.  [ ] Import
2.  [ ] Export
3.  [ ] Password generator
4.  [ ] Password strength
5.  [ ] Remember old passwords with timestamps
6.  [ ] Change master password
7.  [ ] Modify and delete MPM (master) accounts
8.  [ ] Bind return/enter keys to complete action
9.  [ ] Light / Dark mode
10. [ ] Auto scroll to the position after modifying account
11. [ ] Put master password on a timer to prevent entering again and again for
    each modification

## TODO (Low priority)
1. [ ] Synchronize the encrypted data with services like Dropbox, Google
   Drive, etc.
2. [ ] Manage conflicts during synchronization
3. [ ] Browser extension support
4. [ ] Standalone Application
