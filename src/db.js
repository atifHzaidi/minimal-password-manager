export function checkVersion(db) {
    db.onversionchange = (event) => {
        db.close();
        const message = "A new version of this page is ready. Please reload or close this tab!";
        console.warn(message);
        alert(message);
    };
}

export function dbOpen(database, version, successCallback, errorCallback, blockCallback, versionChangeCallback) {
    const request = indexedDB.open(database, version);

    request.onsuccess = event => {
        const db = event.target.result;

        if (versionChangeCallback === undefined)
            checkVersion(db);
        else
            db.onversionchange = event2 => versionChangeCallback(db, event2);

        if (successCallback === undefined)
            console.log(`Opened '${database}' successfully`);
        else
            successCallback(db);
    }

    request.onerror = event => {
        if (errorCallback === undefined)
            console.error(`Unable to open '${database}'`);
        else
            errorCallback(event);
    }

    request.onupgradeneeded = event => {
        const db = event.target.result;

        if (versionChangeCallback === undefined)
            checkVersion(db);
        else
            db.onversionchange = event2 => versionChangeCallback(db, event2);

        console.log(`Upgrading '${database}'`);

        const objectStore = db.createObjectStore("accounts", { keyPath: "name" });
        // objectStore.createIndex("name", "name", { unique: true });

        objectStore.transaction.oncomplete = event2 => {
            if (successCallback === undefined)
                console.log(`Opened '${database}' successfully`);
            else
                successCallback(db);
        };
    }

    request.onblocked = event => {
        if (blockCallback === undefined) {
            const message = "Please close all other tabs with this site open!";
            console.warn(message);
            alert(message);
        } else
            blockCallback(event);
    }
}

export function getStore(db, mode, name) {
    if (name === undefined)
        name = "accounts";

    if (mode === undefined)
        mode = "readonly";

    return db.transaction(name, mode).objectStore(name);
}

export function getDb(blockCallback, versionChangeCallback) {
    return new Promise((resolve, reject) => {
        dbOpen("minimal-password-manager", 3, resolve, reject, blockCallback, versionChangeCallback);
    });
}

export function getAccounts(db) {
    return new Promise((resolve, reject) => {
        const cursor = getStore(db).getAll();
        cursor.onsuccess = event => resolve(event.target.result);
        cursor.onerror = event => reject(event);
    });
}


function Base64ToArrayBuffer(base64) {
    const binary_string =  window.atob(base64);
    const len = binary_string.length;
    let bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}


function ArrayBufferToBase64(buffer) {
    const array = new Uint8Array(buffer);
    const string = array.reduce((acc, x) => acc + String.fromCharCode(x), "");
    const base64string = window.btoa(string);
    return base64string;
}


async function encrypt(data, password, key) {
    if (password.length < 4) throw "Minimum password length required is 4"

    const encoder = new TextEncoder();

    if (!key)
        key = await crypto.subtle.generateKey(
            {
                name: "AES-GCM",
                length: 256,
            },
            true,
            ["encrypt", "decrypt"]
        );
    else if (typeof(key) === "string")
        key = await importKey(key);

    const encrypted_buffer = await crypto.subtle.encrypt(
        {
            name: "AES-GCM",
            iv: encoder.encode(password),
        },
        key,
        encoder.encode(data)
    );

    return {
        data: ArrayBufferToBase64(encrypted_buffer),
        key: ArrayBufferToBase64(await crypto.subtle.exportKey("raw", key)),
    };
}


async function decrypt(encrypted_data, password, key) {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();

    const decrypted_buffer = await crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: encoder.encode(password)
        },
        await importKey(key),
        Base64ToArrayBuffer(encrypted_data),
    );

    return decoder.decode(new Uint8Array(decrypted_buffer));
}

async function importKey(key) {
    return await crypto.subtle.importKey(
        "raw",
        Base64ToArrayBuffer(key),
        "AES-GCM",
        true,
        ["encrypt", "decrypt"]
    )
}


function checkDetails(details) {
    if (!details.name) throw ".name is required";
    if (typeof(details.name) !== "string") throw ".name should be a string";

    if (!details.password) throw ".password is required";
    if (typeof(details.password) !== "string") throw ".password should be a string";

    if (!details.data) details.data = [];
    if (!Array.isArray(details.data)) throw ".data should be an array";
}


export async function addAccount(db, details) {
    checkDetails(details);

    const data = await encrypt(JSON.stringify(details.data), details.password);

    return new Promise((resolve, reject) => {
        const request = getStore(db, "readwrite").add({
            name: details.name,
            email: details.email,
            encrypted_data: data.data,
            key: data.key,
        });

        request.onsuccess = event => resolve(event);
        request.onerror = event => reject(event);
    })
}


export function getAccountDetails(db, name, password) {
    return new Promise((resolve, reject) => {
        const request = getStore(db).get(name);

        request.onsuccess = async event => {
            const result = event.target.result;
            decrypt(result.encrypted_data, password, result.key).then(
                data => resolve(JSON.parse(data)),
                error => reject(error),
            )
        }
        request.onerror = event => reject(event);
    });
}


export function updateAccountDetails(db, details) {
    checkDetails(details);

    return new Promise((resolve, reject) => {
        const request = getStore(db).get(details.name);

        request.onsuccess = async event => {
            const data = event.target.result;

            // Check if the password is valid
            await decrypt(data.encrypted_data, details.password, data.key);

            // Now we encrypt the data using the existing key
            const edata = await encrypt(JSON.stringify(details.data), details.password, data.key);

            // Verify if the keys are same
            if (data.key !== edata.key) throw "Keys don't match";

            data.encrypted_data = edata.data;

            const requestUpdate = getStore(db, "readwrite").put(data);

            requestUpdate.onsuccess = event => resolve(event);
            requestUpdate.onerror = event => reject(event);
        }
    });
}


export function updateAccountPassword(db, name, oldPass, newPass, successCallback, errorCallback) {
    return new Promise((resolve, reject) => {
        const objStore = getStore(db, "readwrite");
        const request = objStore.get(name);

        if (successCallback) request.onsuccess = async event => {
            const result = event.target.result;
            const data = await decrypt(result.encrypted_data, oldPass, result.key);
            const edata = await encrypt(data, newPass);
            result.encrypted_data = edata.data;
            result.key = edata.key;

            const requestUpdate = objStore.put(result);

            requestUpdate.onsuccess = event => resolve(event);
            requestUpdate.onerror = event => reject(event);
        }
    });
}


export async function updateAccountData(name, password, data) {
    const db = await getDb();

    await updateAccountDetails(db, { name, password, data });

    const accounts = await getAccountDetails(db, name, password);

    db.close();

    return accounts;
}
