import { writable } from "svelte/store";
import Login from './Login.svelte';
import Accounts from './Accounts.svelte';

export function createRouter(defaultComponent) {
    if (defaultComponent === undefined)
        defaultComponent = null;

    const { subscribe, set, update } = writable({
        currentComponent: defaultComponent,
        routes: {
            'Login': Login,
            'Accounts': Accounts,
        },
        props: {},
    });

    return {
        subscribe,
        setRoute: (name, props) => update(r => {
            if (name in r.routes) r.currentComponent = r.routes[name];
            else throw `${name} not a valid route`;

            if (props && props.constructor !== Object) throw "props must be an object";

            r.props = props || {};

            return r;
        }),
    };
}

export const router = createRouter();
